# Main class that compares and analyzes data sets
class Analyzer:

    def __init__(self, in_list_1, in_list_2):
        self.list_1 = in_list_1
        self.list_2 = in_list_2
        self.id_field = 'id'
        self.price_field = 'price'
        self.retail_field = 'retail'
        self.snaptravel_field = 'snaptravel'

    # Transform a list into a dictionary, where the keys are the record ids
    def __index_by_id(self, record_list):
        record_dic = {}
        for record in record_list:
            record_dic[record[self.id_field]] = record
        return record_dic

    # Helper function to retrieve common ids
    def __get_common_record_ids(self):
        set_1 = self.__index_by_id(self.list_1)
        set_2 = self.__index_by_id(self.list_2)
        common_record_ids = set(set_1).intersection(set(set_2))
        return common_record_ids

    # Get common records between the 2 sets
    # TODO: Update this method to compare an arbitrary number of sets
    def get_common_records(self):
        common_records = list()
        common_record_ids = self.__get_common_record_ids()

        set_1 = self.__index_by_id(self.list_1)
        set_2 = self.__index_by_id(self.list_2)

        for i in common_record_ids:
            price_1 = set_1[i][self.price_field]
            price_2 = set_2[i][self.price_field]
            record = set_1[i]
            del record[self.price_field]
            record[self.price_field] = {
                self.snaptravel_field: price_1,
                self.retail_field: price_2
            }
            common_records.append(record)

        return common_records
