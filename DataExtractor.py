import requests
import json
import xmltodict


# Class that retrieves data from the data sources via REST calls
class DataExtractor:

    def __init__(self):
        self.DEFAULT_PROVIDER = 'snaptravel'
        self.hotels_end_point = 'https://experimentation.getsnaptravel.com/interview/hotels'
        self.legacy_hotels_end_point = 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'

    def get_hotel_data(self, city, check_in, check_out):
        body = {
            'city': city,
            'checkin': check_in,
            'checkout': check_out,
            'provider': self.DEFAULT_PROVIDER
        }
        r = self.make_post_request(self.hotels_end_point, data=body)
        response = json.loads(r)

        # Stub used to test this exercise
        # response = self.__stub_response_hotel_data()

        return response

    def get_legacy_hotel_data(self, city, check_in, check_out):
        xml_body_template = """
        	<?xml version="1.0" encoding="UTF-8"?>
        		<root>
        			<checkin>checkin_string_input</checkin>
        			<checkout>checkout_string_input</checkout>
        			<city>city_string_input</city>
        			<provider>snaptravel</provider>
				</root>
		"""

        body = xml_body_template.replace('city_string_input', city).replace('checkin_string_input', check_in).replace(
            'checkout_string_input', check_out)
        r = self.make_post_request(self.legacy_hotels_end_point, data=body)

        # Stub used to test XML parsing
        # r = self.__stud_xml_response()

        response = json.loads(json.dumps(xmltodict.parse(r)))['root']['element']

        # Stub used to test this exercise
        # response = self.__stub_response_legacy_hotel_data()

        return response

    # Helper function to submit post requests
    def make_post_request(self, url, data):
        r = requests.post(url=url, data=data)
        return r

    # Stub used to test XML parsing
    def __stud_xml_response(self):
        r = """<?xml version="1.0" encoding="UTF-8"?>
        <root>
          <element>
            <id>12</id>
            <hotel_name>Center Hilton</hotel_name>
            <num_reviews>209</num_reviews>
            <address>12 Wall Street, Very Large City</address>
            <num_stars>4</num_stars>
            <amenities>
              <element>Wi-Fi</element>
              <element>Parking</element>
            </amenities>
            <image_url>https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg</image_url>
            <price>132.11</price>
          </element>
        </root>
        """
        return r

    # Stub response for hotel data to help develop this exercise
    def __stub_response_hotel_data(self):
        response = [
            {
                'id': 12,
                'hotel_name': 'Center Hilton',
                'num_reviews': 209,
                'address': '12 Wall Street, Very Large City',
                'num_stars': 4,
                'amenities': ['Wi-Fi', 'Parking'],
                'image_url': 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
                'price': 132.11
            },
            {
                'id': 10,
                'hotel_name': 'Other Hotel',
                'num_reviews': 150,
                'address': 'Some Street, Some City',
                'num_stars': 3,
                'amenities': ['Parking'],
                'image_url': 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/other_pic.jpg',
                'price': 78.89
            },
            {
                'id': 20,
                'hotel_name': 'Worst Hotel Ever',
                'num_reviews': 1000,
                'address': 'Middle of nowhere, Lost City',
                'num_stars': 1,
                'amenities': [],
                'image_url': 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/worst_pic.jpg',
                'price': 0.99
            }
        ]
        return response

    # Stub response for legacy data to help develop this exercise
    def __stub_response_legacy_hotel_data(self):
        response = [
            {
                'id': 12,
                'hotel_name': 'Center Hilton',
                'num_reviews': 209,
                'address': '12 Wall Street, Very Large City',
                'num_stars': 4,
                'amenities': ['Wi-Fi', 'Parking'],
                'image_url': 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
                'price': 128.75
            },
            {
                'id': 10,
                'hotel_name': 'Center Hilton',
                'num_reviews': 209,
                'address': '12 Wall Street, Very Large City',
                'num_stars': 4,
                'amenities': ['Wi-Fi', 'Parking'],
                'image_url': 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
                'price': 100.95
            }
        ]
        return response
