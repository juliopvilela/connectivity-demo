import sys
from DataExtractor import DataExtractor
from Analyzer import Analyzer
from DatabaseDao import DatabaseDao
from concurrent.futures import ThreadPoolExecutor, as_completed


# Entry point to the exercise
def main(in_city, in_check_in, in_check_out):

    # Make 2 HTTP Post requests in parallel
    print "Making 2 http concurrent calls..."
    data_extractor = DataExtractor()
    pool = ThreadPoolExecutor(1)
    futures = [
        pool.submit(data_extractor.get_hotel_data, in_city, in_check_in, in_check_out),
        pool.submit(data_extractor.get_legacy_hotel_data, in_city, in_check_in, in_check_out)
    ]
    resp_1, resp_2 = [r.result() for r in as_completed(futures)]

    # Look for common elements in the responses
    print "Comparing responses..."
    analyzer = Analyzer(resp_1, resp_2)
    common_records = analyzer.get_common_records()
    print "Count of common records: " + str(len(common_records))

    # Write out results to a DB
    # TODO: Load database credentials from a configuration file
    print "Writing results to database"
    db = DatabaseDao(user_name="user", user_password="pass")
    db.insert_common_records(common_records)
    db.close()


if __name__ == '__main__':
    city = sys.argv[1]
    check_in = sys.argv[2]
    check_out = sys.argv[3]
    main(city, check_in, check_out)
