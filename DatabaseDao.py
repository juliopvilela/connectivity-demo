import psycopg2
import json


# Data Access Object for the Records table
class DatabaseDao:

    def __init__(self, user_name, user_password):
        self.db_host = "db_host"
        self.db_name = "db_name"
        self.records_table = "json_records"
        self.user_name = user_name
        self.user_password = user_password
        self.conn = psycopg2.connect(host=self.db_host, dbname=self.db_name, user=self.user_name,
                                     password=self.user_password)

    # Helper function to execute SQL operations
    def __execute(self, query, args):
        cur = self.conn.cursor()
        cur.execute(query, args)
        self.conn.commit()
        rows = cur.fetchall
        return rows

    # Insert common records to a table in a database
    # TODO: Might want to consider a query builder to avoid: a) hardcoding SQL statements and b) avoid SQL injection.
    def insert_common_records(self, records):
        for record in records:
            json_record = json.dumps(record)
            query = """ INSERT into json_records (record) values ('{0}')""".format(json_record)
            self.__execute(query, {})

    def close(self):
        self.conn.close()
